package nikolaychuks.market.marketinventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketinventoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketinventoryApplication.class, args);
    }

}
